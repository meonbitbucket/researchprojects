#include <modbus.h>
#include <errno.h>
#include <iostream>



int main()
{

	modbus_t* context = modbus_new_tcp("192.168.0.131", 502);
	
	if (context == NULL)
		std::cout << "unable to allocate libmodbus context\n";

	if (modbus_connect(context) == -1)
		std::cout << "connection failed: " << modbus_strerror(errno) << "\n";
	else
		std::cout << "connection successful!\n";

	if (modbus_set_slave(context, 1) == -1)
		std::cout << "could not set slave number\n";
		

	uint16_t register_in[10];

	int rc = modbus_read_registers(context, 0, 10, register_in);	 
	if (rc == -1)
		std::cout << "error: " << modbus_strerror(errno) << " errno: " << errno << "\n";
	else
		for (int i = 0; i < 10; i++)
			std::cout << "register" << i << " is " << register_in[i] << "\n" ;

	uint16_t register_out[10];
	std::fill_n(register_out, 10, 5);
	rc = modbus_write_registers(context, 0, 10, register_out);
	if (rc != -1)
		std::cout << "forced registers 0 to 9 to 5\n";
	else
		std::cout << "error registers could not be forced." << modbus_strerror(errno) << "\n";
	

	return 0;
	
}
	
