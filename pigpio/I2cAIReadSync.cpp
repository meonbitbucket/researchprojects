#include "pigpio.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <chrono>
#include<bitset>
#include <pthread.h>
#include <mutex>
using namespace std;
using namespace std::chrono;

std::mutex mtx;

void* poll_channel2(void*);
void convert_to_bit_set(const char*, bitset<8>*);
void* read(void*);
double convert_to_double(const bitset<8>*);

double convert_to_double(const bitset<8>* data_array)
{
	string data_bit_str = data_array[0].to_string() + data_array[1].to_string();

	data_bit_str = data_bit_str.erase(0, 4); //delete redundant 4 bits
	bitset<12> data(data_bit_str);

		if (data[11] == 1)
		{
			data.flip();
			for(int i=0; i<data.size(); ++i)
			{
				if (data[i]) { data[i] = 0;}
				else { data[i] = 1; break; }
			}
		}

		double convert = double(data.to_ulong())/1000.0;
		convert = convert*5.56;
		return convert;
	
	/*	high_resolution_clock::time_point t2= high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
		AIStream << std::fixed << std::setprecision(13) << value << "; blocking time: " << duration << std::endl; */
}



void* read(void* args)
{
	gpioInitialise();
	int channel = *(int*) args;
	char* config_byte;
	 
	if (channel == 1) 
		config_byte = "\x80";
	else
		config_byte= "\xa0";

	int handle = i2cOpen(1, 0x68, 0);
	
	char in[3];
	mtx.lock();	
	char bytesWritten = i2cWriteDevice(handle, config_byte, 1); //configuration register with 12 bit, one-shot
	char bytesRead = i2cReadDevice(handle, in, 3);
		
	bitset<8> data_array[3];
	convert_to_bit_set(in, data_array);
	cout << "channel " << channel << ": " << convert_to_double(data_array) << " " << data_array[2] << endl;

	while (data_array[2][7])
	{
		i2cReadDevice(handle, in, 3);
		convert_to_bit_set(in, data_array);
	}
	mtx.unlock();		
	cout << "channel " << channel << ": " << convert_to_double(data_array) << " " << *(data_array+2) << endl;	
	i2cClose(handle);
}		

void convert_to_bit_set(const char* data, bitset<8>* data_array) //dont want to change the data
{

	for(int i=0; i<3; i++)
	{
		bitset<8> byte(*data);
		data_array[i] = byte;
		++data;
	}

	return;
}

void* poll_channel2(void* args)
{
	int channel = 2;
	for(int i=0; i<50; i++)
	{
		read( (void*) &channel );
		usleep(10*1000);
	}
}


int main()
{
	gpioInitialise();
	pthread_t thread1, thread2;

	int thread_handle = pthread_create(&thread1, NULL, poll_channel2, NULL);

	int channel = 1;
	

	for (int i=0 ; i<50 ; i++)
	{
		read( (void*) &channel ); 
		usleep(10*1000);
	}
		
	//wait for the other read thread to finish
	pthread_join(thread1, NULL);

	gpioTerminate();


	return 0;	
}

	

	
